# OpenML dataset: PriceRunner

https://www.openml.org/d/45714

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

These datasets originate from PriceRunner, a popular product comparison platform. They contain product-related information including product IDs, titles, and categories. It can be used for numerous tasks, such as classification, clustering, record linkage, etc.

Column description:
  * Product ID
  * Product Title as it appears in the respective product comparison platform (lower case and with punctuation removed)
  * Vendor ID: the ID of the electronic store that provides the product.
  * Cluster ID: the ID of the cluster that the product belongs to. Useful for entity matching and clustering tasks.
  * Cluster Label: The title of the aforementioned cluster.
  * Category ID: the ID of the category that the product belongs to. Useful for classification and categorization tasks.
  * Category Label: The title of the aforementioned category.

Citations:
  * L. Akritidis, A. Fevgas, P. Bozanis, C. Makris, "A Self-Verifying Clustering Approach to Unsupervised Matching of Product Titles", Artificial Intelligence Review (Springer), pp. 1-44, 2020.
  * L. Akritidis, P. Bozanis, "Effective Unsupervised Matching of Product Titles with k-Combinations and Permutations", In Proceedings of the 14th IEEE International Conference on Innovations in Intelligent Systems and Applications (INISTA), pp. 1-10, 2018.
  * L. Akritidis, A. Fevgas, P. Bozanis, "Effective Product Categorization with Importance Scores and Morphological Analysis of the Titles", In Proceedings of the 30th IEEE International Conference on Tools with Artificial Intelligence IICTAI), pp. 213-220, 2018.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45714) of an [OpenML dataset](https://www.openml.org/d/45714). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45714/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45714/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45714/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

